/*************************************************************************
  > File Name: test.c
  > company: GDOU
  > Mail: lsy476941913@live.com
  > Created Time: 2017年03月日 星期四 20时36分17秒
 ************************************************************************/
#include "ts.h"
extern int ts_fd;



int
init_ts(void)
{
	ts_fd = open("/dev/event0",O_RDONLY);
	if (ts_fd == -1) {
		perror("open ts");
		return -1;
	}
	return 0;
}

	int
get_pos(int *x,int *y,bool *release)
{

	struct input_event ts;
	bool x_ready=false;
	bool y_ready=false;

	*release = false;

	
	int count = 0;
	while (1) {
		read(ts_fd,&ts,sizeof(ts));		//读取屏幕触摸的事件
		if (ts.type == EV_ABS) {		//判断是否屏幕发生了事件触发
			if (ts.code == ABS_PRESSURE || ts.value == 0) {	//遇到触摸弹起信号，退出触摸坐标获取
				*release = true;
				break ;
			}
			if (ts.code == ABS_X) {		//接受到X坐标
				*x = ts.value;
				x_ready = true;
				count ++;
			}
			if (ts.code == ABS_Y) {		//接受到Y坐标
				*y =ts.value;
				y_ready = true;
				count ++;
			}
			
			// if(count == 2){
			// 	count = 0;
			// 	break ;
			// }
			if(x_ready == true && y_ready == true){
				printf("x:%d,y:%d\n",*x,*y);
				break ;
			}

		}

	}
	return 0;
}
void 
lose_ts(int ts_fd)
{
	close(ts_fd);
}