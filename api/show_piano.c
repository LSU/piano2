/*************************************************************************
    > File Name: test.c
	> company: GDOU
    > Mail: lsy476941913@live.com 
    > Created Time: 2017年03月6日 星期四 20时36分17秒
 ************************************************************************/
#include "show_piano.h"
#include "show_bmp.h"
#include "lcd.h"
#include "unistd.h"
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include "ts.h"
 
 extern int fbfd;
 extern uint8_t key_status[12];	//1-12个按钮的状态。
 extern bool key_first_time[12];
 extern pthread_t p_mp3_id;

int 
show_piano(void){

	show_bmp_picture(fbfd, "img/bmp/background.bmp",0,0);
	show_bmp_picture(fbfd, "img/bmp/bar.bmp",0,0);
	int i=0;
	for(i=0;i<12;i++){
		show_bmp_picture(fbfd, "img/bmp/key_off.bmp",65*i+8,180);
	}

	return 0;
}

int 
show_key_off(uint8_t key_num)
{
	show_bmp_picture(fbfd, "img/bmp/key_off.bmp",65 * key_num + 8,180);
	if(p_mp3_id != 0){
		
		//system("killall madplay");
		//key_status[key_num] = KEY_OFF;
	}
	return 0;
}

/*
void 
thread_show_bmp(uint8_t key_num)
{
	pthread_detach(pthread_self());
	show_bmp_picture(fbfd, "img/bmp/key_on.bmp", 65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on1.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on2.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on2.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on1.bmp",65 * key_num + 8,180);

	show_bmp_picture(fbfd, "img/bmp/key_off.bmp",65 * key_num + 8,180);

	sleep(0.3);
	key_status[key_num] = KEY_OFF;

	return ;
}
int 
show_key_on(uint8_t key_num)
{
	pthread_t id;
	int ret;
	ret = pthread_create(&id,NULL,(void *)thread_show_bmp,(uint8_t *)&key_num);
	if(ret != 0) {
		printf ("Create pthread show_bmp error!\n");
		exit (1);
	}
	//pthread_detach(id);
	//pthread_join(id,NULL);
	return (0);
} */


int 
show_key_on(uint8_t key_num)
{
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	show_bmp_picture(fbfd, "img/bmp/key_on3.bmp",65 * key_num + 8,180);
	return 0;
} 
