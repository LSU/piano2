/*************************************************************************
    > File Name: test.c
	> company: GDOU
    > Mail: lsy476941913@live.com 
    > Created Time: 2017年03月6日 星期四 20时36分17秒
 ************************************************************************/
#include <stdio.h>
#include <string.h>
#include "mp3_play.h"
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

extern char *vol;



 
void 
thread_mp3_play(const char *mp3)
{
	pthread_detach(pthread_self());
	char *str;
	char *temp;
	temp = (char *)malloc(sizeof(char)*50);
	str = (char *)malloc(sizeof(char)*100);
	memset(temp,0,20);
	memset(str,0,sizeof(50));

	strcpy(temp," ");
	strcat(temp,mp3);
	
	strcat(temp," &");
	
	strcpy(str,"/bin/madplay -Q -i  ");
	strcat(str,vol);
	strcat(str,temp);

	//strcat(str,"; echo `ps -ef | grep \"madplay\" | grep -v \"grep\" | grep -v \"start\" | awk '{print $2}'`");
	printf("\"cmd:%s\n\"", str);

	system(str);
	free(str);
	free(temp);
}
int 
music_play(const char *mp3_path)
{
	if(p_mp3_id != 0)
	{
		system("killall madplay");
		//system("ps -ef | grep \"madplay\" | grep -v \"grep\" | grep -v \"start\" | awk '{print $2}' | kill");
		//pthread_kill(p_mp3_id, SIGKILL);
		//pthread_cancel(p_mp3_id);
	}
	printf("\n\n%s\n\n",mp3_path);
	pthread_t id;
	int ret;
	ret = pthread_create(&id,NULL,(void *)thread_mp3_play,(char *)mp3_path);
	if (ret != 0) {
		printf ("Create pthread mp3_play error!\n");
		exit (EXIT_FAILURE);
	}
	p_mp3_id=id;
	//pthread_join(id,NULL);
	return (0);
} 
