/*************************************************************************
    > File Name: test.c
	> company: GDOU
    > Mail: lsy476941913@live.com 
    > Created Time: 2017年03月7日 星期四 20时36分17秒
 ***************************************************************static *********/
 #include "lcd.h"
 
extern int fbfd ;


int 
lcd_init(void)
{
	

	fbfd = open("/dev/fb0", O_RDWR);
	if (!fbfd) {
		printf("Error: cannot open framebuffer device.\n");
		exit(1);
	}
	return 0;
	
}

int 
lcd_close(void)
{
	close(fbfd);
	return 0;
}