/*************************************************************************
    > File Name: test.c
	> company: GDOU
    > Mail: lsy476941913@live.com
    > Created Time: 2017年03月5日 星期四 20时36分17秒
 ************************************************************************/

#include <stdio.h>
#include "lcd.h"
#include "show_bmp.h"
#include "ts.h"
#include "decide_pos.h"
#include "show_piano.h"
#include "mp3_play.h"
#include "argopt.h"

extern uint8_t key_status[12];
extern 	int ts_fd;
extern pthread_t p_mp3_id;
extern bool release;
extern bool pre_release;
extern int new_key;
extern int old_key;
extern bool first_time;
extern const char *music_set[12];

int key_num;
	
int
main(int argc,char **argv)
{
	int x=0;
	int y=0;
	release  	= false;
	//pre_release 	= true;
	p_mp3_id 	= 0;
	new_key  	= 10;
	old_key 		= 10;
	argopt(argc,argv,"as");
	lcd_init();
	show_piano();

	init_ts();
	
	while (1) {
		int i;
		old_key = new_key ;	
		release = false;
  		get_pos(&x,&y,&release);
  		if(release)
		{
			for(i=0;i<12;i++)
				show_key_off(i);
			first_time = true;
		}
		if(x > 785 || x <5 || y> 478 || y <= 200)
		{
			continue;
		}

		if(release)
		{
			show_key_off((x-8)/65);
			first_time = true;

			continue;
		}
		
		new_key = (x-8)/65;

		if(new_key != old_key || first_time)
		{
			show_key_on(new_key);
			if(!first_time)
				show_key_off(old_key);
			else
				first_time = false;	
			music_play(music_set[new_key]);
		}
		
	}
	close_ts(ts_fd);
	return 0;
}

