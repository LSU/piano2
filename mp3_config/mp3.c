/*************************************************************************
    > File Name: test.c
	> company: GDOU
    > Mail: lsy476941913@live.com 
    > Created Time: 2017年03月6日 星期四 20时36分17秒
 ************************************************************************/
const char mp3_1[]  = "mp3/d1.mp3";
const char mp3_2[]  = "mp3/d2.mp3";
const char mp3_3[]  = "mp3/d3.mp3";
const char mp3_4[]  = "mp3/d4.mp3";
const char mp3_5[]  = "mp3/d5.mp3";
const char mp3_6[]  = "mp3/d6.mp3";
const char mp3_7[]  = "mp3/d7.mp3";
const char mp3_8[]  = "mp3/d8.mp3";
const char mp3_9[]  = "mp3/d9.mp3";
const char mp3_10[] = "mp3/d10.mp3";
const char mp3_11[] = "mp3/d11.mp3";
const char mp3_12[] = "mp3/d12.mp3";

const char *music_set[] = {
	mp3_1,
	mp3_2,
	mp3_3,
	mp3_4,
	mp3_5,
	mp3_6,
	mp3_7,
	mp3_8,
	mp3_9,
	mp3_10,
	mp3_11,
	mp3_12
};
