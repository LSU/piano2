#ifndef __SHOW_BMP_H
#define __SHOW_BMP_H
/*************************************************************************
    > File Name: test.c
    >Author :Simon
		> company: GDOU
    > Mail: lsy476941913@live.com 
    > Created Time: 2017年03月5日 星期四 20时36分17秒
 ************************************************************************/ 


 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <errno.h>

#define PIANO_KEY_WIDTH 65
#define PIANO_KEY_HEIGHT 280


#define KEY_ONE		0
#define KEY_TWO		1
#define KEY_THREE	2
#define KEY_FOUR		3
#define KEY_FIVE		4
#define KEY_SIX		5
#define KEY_SEVEN	6
#define KEY_EIGHT	7
#define KEY_NINE		8
#define KEY_TEN		9
#define KEY_ELEVEN	10
#define KEY_TWELVE	11

#define KEY_ON		1
#define KEY_OFF		0

uint8_t key_status[12];	//1-12个按钮的状态。

/**
 *
 *14byte文件头
 *
 */ 
typedef struct 
 {
	
char cfType[2];	//文件类型，"BM"(0x4D42)
	int32_t cfSize;		//文件大小（字节）
	int16_t cfReserved1;	//保留，值为0
	int16_t cfReserved2;	//保留，值为0
	int32_t cfoffBits;	//数据区相对于文件头的偏移量（字节）
} __attribute__ ((packed)) BITMAPFILEHEADER;
 
 
/**
 *
 *40byte信息头
 *
 */ 
typedef struct 
 {
	
char ciSize[4];	//BITMAPFILEHEADER所占的字节数
	uint32_t ciWidth;	//宽度
	uint32_t ciHeight;	//高度
	char ciPlanes[2];	//目标设备的位平面数，值为1
	int16_t ciBitCount;	//每个像素的位数
	char ciCompress[4];	//压缩说明
	char ciSizeImage[4];	//用字节表示的图像大小，该数据必须是4的倍数
	char ciXPelsPerMeter[4];	//目标设备的水平像素数/米
	char ciYPelsPerMeter[4];	//目标设备的垂直像素数/米
	char ciClrUsed[4];	//位图使用调色板的颜色数
	char ciClrImportant[4];	//指定重要的颜色数，当该域的值等于颜色数时（或者等于0时），表示所有颜色都一样重要
} __attribute__ ((packed)) BITMAPINFOHEADER;

typedef struct 
 {
	
unsigned char blue;	//蓝色
	unsigned char green;	//绿色
	unsigned char red;	//红色
	unsigned char reserved;	//APACHE
} __attribute__ ((packed)) PIXEL;	//颜色模式RGBA


BITMAPFILEHEADER FileHead;
BITMAPINFOHEADER InfoHead;
 
 
 
 
//  static void 
//fb_update(int fd,struct fb_var_screeninfo *vi);   //将要渲染的图形缓冲区的内容绘制到设备显示屏来


int 
bitmap_format_convert(int width, int height, uint32_t * dst, char *src);
 
void 
draw_point(char *fb_mem, int x, int y, uint32_t color);
 
void 
draw_rect_fill(char *fb_mem, int x, int y, int width, int height,
			uint32_t * bmp_src);
 
int 
show_bmp(char *path, int x, int y);
 
int 
show_bmp_picture(int fd, char *path,int x,int y);


#endif //__SHOW_BMP_H