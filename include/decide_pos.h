#ifndef DECIDE_POS_H_INCLUDED
#define DECIDE_POS_H_INCLUDED

/*************************************************************************
    > File Name: lsy.c
    > Author:Simon
    > company: GDOU
    > Mail: lsy476941913@live.com
    > Created Time: 2017年03月06日 星期一 23时31分04秒
 ************************************************************************/

#include<stdio.h>
#include<stdint.h>
#include "ts.h"


void set_key(uint8_t key_num);
int decide_pos(void);


#endif // DECIDE_POS_H_INCLUDED
