#ifndef __SHOW_PIANO_H
#define __SHOW_PIANO_H
/*************************************************************************
  > File Name: test.c
  > company: GDOU
  > Mail: lsy476941913@live.com 
  > Created Time: 2017年03月7日 星期四 20时36分17秒
 ************************************************************************/
#include<stdint.h>
#include<sys/types.h>
int 
show_piano(void);

int 
show_key_off(uint8_t  key_num);

int 
show_key_on(uint8_t  key_num);
#endif // __SHOW_PIANO_H

