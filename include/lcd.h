#ifndef __LCD_H
#define __LCD_H
/*************************************************************************
    > File Name: test.c
	  > company: GDOU
    > Mail: lsy476941913@live.com 
    > Created Time: 2017年03月7日 星期四 20时36分17秒
 ************************************************************************/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <errno.h>

int fbfd;

int 
lcd_init(void);
int 
lcd_close(void);

#endif  //__LCD_H