#ifndef __TS_H
#define __TS_H
/*************************************************************************
    > File Name: test.c
	> company: GDOU
    > Mail: lsy476941913@live.com 
    > Created Time: 2017年03月7日 星期四 20时36分17秒
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
bool release;
bool pre_release;
int ts_fd;
int new_key;
int old_key;

bool first_time;

int
init_ts(void);
int
get_pos(int *x,int *y,bool *release);
void 
close_ts(int ts_fd);

#endif //__TS_H