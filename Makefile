#/*************************************************************************
#    > File Name: test.c
#    > company: GDOU
#    > Mail: lsy476941913@live.com
#    > Created Time: 2017年03月5日 星期四 20时36分17秒
# ************************************************************************/
CC = arm-linux-gcc
LD = arm-linux-ld

OBJCOPY = arm-linux-objcopy
INCLUDEDIR := $(shell pwd)/include/
CPPFLAGS := -I$(INCLUDEDIR)
CFLAGS := -Wall 

export CC LD OBJCOPY CPPFLAGS CFLAGS

OBJS := index.o \
	api/lcd.o api/show_bmp.o api/ts.o api/decide_pos.o \
	api/show_piano.o api/mp3_play.o mp3_config/mp3.o api/argopt.o \
	

start:$(OBJS)
	$(CC) $(OBJS) -o start -lpthread
	cp start  ~/nfs/

%.o:%.S
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@
%.o:%.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

clean:
	rm ./*.o
	make clean -C api


